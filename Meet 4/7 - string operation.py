isi_string = " aku dan dia tidak akan bersama "

print(isi_string.capitalize())
print(isi_string.upper())
print(isi_string.lower())
print(isi_string.find("akan"))
print(isi_string.isalpha()) # fill with character only
print(isi_string.isalnum()) # fill with numeric character only # character still count as number
print(len(isi_string))
print(isi_string.replace("tidak", "selalu"))
print(isi_string.strip())

bikin_list = isi_string.split(" ")
print(bikin_list)