import sys
import random
import os

class Hewan:
    # buat variabel dengan
    __nama = None
    __tinggi = None
    __berat = None
    __suara = None

    # Konstruktor berguna untuk menginisialisasi objek
    # self berguna untuk merujuk dirinya sendiri dalam objek
    def __init__(self, nama, tinggi, berat, suara):
        self.__nama = nama
        self.__tinggi = tinggi
        self.__berat = berat
        self.__suara = suara

    def set_nama(self, nama):
        self.__nama = nama
    
    def set_tinggi(self, tinggi):
        self.__tinggi = tinggi
    
    def set_berat(self, berat):
        self.__berat = berat
    
    def set_suara(self, suara):
        self.__suara = suara

    def get_nama(self):
        return (self.__nama)
    
    def get_tinggi(self):
        return str(self.__tinggi)
    
    def get_berat(self):
        return str(self.__berat)
    
    def get_suara(self):
        return (self.__suara)

    def get_type(self):
        print("Hewan")

    def toString(self):
        return "{} tingginya {} cm dan beratnya {} kilogram dan bersuara {}".format(self.__nama, self.__tinggi, self.__berat, self.__suara)

# cara membuat objek hewan yaitu kucing
kucing = Hewan('Kucing', 33, 10, 'Meong')

# merubah tinggi kucing
kucing.set_tinggi(35)

# menampilkan fungsi print yang sudah dibuat di objek
print(kucing.toString())

# menampilkan suara dari kucing
print(kucing.get_suara())