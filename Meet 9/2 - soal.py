import sys
import random
import os

class kendaraan:
    # buat variabel dengan
    __nama = None
    __roda = None
    __kursi = None

    # Konstruktor berguna untuk menginisialisasi objek
    # self berguna untuk merujuk dirinya sendiri dalam objek
    def __init__(self, nama, roda, kursi):
        self.__nama = nama
        self.__roda = roda
        self.__kursi = kursi

    def set_nama(self, nama):
        self.__nama = nama
    
    def set_roda(self, roda):
        self.__roda = roda
    
    def set_kursi(self, kursi):
        self.__kursi = kursi
    
    def get_nama(self):
        return (self.__nama)
    
    def get_roda(self):
        return str(self.__roda)
    
    def get_kursi(self):
        return str(self.__kursi)
    
    def get_type(self):
        print("Kendaraan")

    def toString(self):
        return "{} rodanya {} buah dan kursinya {} buah".format(self.__nama, self.__roda, self.__kursi)

# cara membuat objek kendaraan yaitu kendaraan 1, 2, 3
kendaraan1 = kendaraan(input(), input(), input())
kendaraan2 = kendaraan(input(), input(), input())
kendaraan3 = kendaraan(input(), input(), input())

# menampilkan fungsi print yang sudah dibuat di objek
print(kendaraan1.toString())
print(kendaraan2.toString())
print(kendaraan3.toString())
