# buat variable berisi array kosong
angka = []

# prompt user input
print("mau berapa kali?")
times = int(input())
print("input!")
for i in range (0,times):
	angka.append(int(input()))

# replace even number with 6
for i in range (0,len(angka)):
	if (angka[i] % 2 == 0):
		angka[i] = 6

# sorting array
for i in range (0,len(angka)):
	for j in range (i+1,len(angka)):
		if(angka[i]>angka[j]):
			temp=angka[i]
			angka[i]=angka[j]
			angka[j]=temp

# print sorting result
print("hasil sorting:")
for i in range (0,len(angka)):
		print(angka[i])

# print odd number of array
print("hanya ganjil:")
for i in range (0,len(angka)):
	if (angka[i] % 2 == 1):
		print(angka[i])
