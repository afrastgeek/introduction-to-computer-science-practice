import tkinter
window = tkinter.Tk()

e1 = tkinter.Entry(window)
e2 = tkinter.Entry(window)
l = tkinter.Label(window)

def penjumlahan():
    total = int(e1.get()) + int(e2.get())
    l.config(text = "hasil = %s" % total)

b = tkinter.Button(window, text = "jumlahkan", command = penjumlahan)

e1.pack()
e2.pack()
l.pack()
b.pack()
b.mainloop()
