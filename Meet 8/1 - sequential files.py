import sys
import random
import os

# Untuk Overwrite atau membuat file baru untuk ditulis
test_file = open("test.txt", "wb")

# Untuk mengetahui mode file yang digunakan
print(test_file.mode)

# Untuk mengetahui nama file yang digunakan
print(test_file.name)

# Menulis tulisan ke dalam file
test_file.write(bytes("Write me to the file\n", 'UTF-8'))

# Menutup file
test_file.close()