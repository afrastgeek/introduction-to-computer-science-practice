# Buat list yang terdiri dari beberapa angka
# jika angka di list genap maka masukan ke dalam file
# jika ganjil jangan masukan ke file
# 
# Contoh:
# 4 <- Jumlah isi list
# 1
# 2
# 3
# 4
# keluaran di file:
# 2
# 4
import sys
import random
import os

num = int(input())

list = []
store = open("output.txt", "wb")

for x in range(0,num):
    list.append(int(input()))
    if (list[x] % 2 == 0):
        store.write(bytes("%d\n" %(list[x]), 'UTF-8'))

store.close()
