import sys
import random
import os

# buka file untuk membaca dan menulis
test_file = open("test.txt", "r+")

# membaca tulisan di file
#   text_in_file = test_file.read()
text_in_file = test_file.readline()

print(text_in_file)

# menutup file
test_file.close()