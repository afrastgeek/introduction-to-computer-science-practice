# Introduction to Computer Science Practice

This is a bunch of source code i write for Practice Class of Introduction to
Computer Science Course at UPI.

The Course are using Python Language.

## File Structure

- Meet 1 - Introduction to Python
- Meet 2 - If Else, For Loop, While Loop, and Function Introduction
- Meet 3 - List and Tuple
- Meet 4 - String
- Meet 5 - Manual Find and Shorting Algorithm
- Meet 6 - Dictionary
- Meet 7 - Function
- Meet 8 - Sequential Files
- Meet 9 - Object Oriented Programming
- Meet 10 - Super Class
- Meet 11 - Simple GUI